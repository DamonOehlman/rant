var server = require('./server');

/**
  # rant

  It's a super hacky, but fun way to present (well it will be)

  ## Example Usage

  Basically, write a simple text file (something like the rant I'm currently
  working on: https://gist.github.com/DamonOehlman/6693531

  Then once you have that, you just pipe the input into rant:

  ```
  cat RANT | rant
  ```

  Heck, you can even curl that sucker:

  ```
  curl https://gist.github.com/DamonOehlman/6693531/raw | rant
  ```

**/

module.exports = function(input, callback) {

  // ensure we have a callback
  callback = callback || function() {};

  server.start(input, function(err, url, svr) {
    if (err) {
      return callback(err);
    }

    process.stderr.write(url);
    callback(err, url, svr);
  });
};