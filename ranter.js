var pull = require('pull-core');
var defaults = require('cog/defaults');
var pos = require('pos');
var tagger = new pos.Tagger();
var lexer = new pos.Lexer();

module.exports = pull.Source(function(input, config) {
  var sections = input.split(/\n\n\s*/);
  var delays = config.delays;
  var counter = 0;

  return function(end, cb) {
    var nextSection;

    function send(data, type, delay) {
      cb(null, {
        first: 0 === counter++,
        type: type,
        data: data || type,
        delay: delay || (type ? (delays[type] || delays.fallback) : delays.word)
      });
    }

    // check for end
    end = end || sections.length === 0;
    if (end) {
      return cb && cb(end);
    }

    nextSection = sections.shift();

    // if this is a special section, then send it across as is
    if (nextSection.charAt(0) === '$') {
      return send(nextSection, nextSection.split('\n')[0]);
    }

    return send(tagger.tag(lexer.lex(nextSection)), '$section');
  };
});

