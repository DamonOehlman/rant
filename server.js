const debug = require('debug')('rant');
const browserify = require('browserify');
const fs = require('fs');
const path = require('path');
const http = require('http');
const mapleTree = require('mapleTree');
const pull = require('pull-stream');
const sse = require('pull-sse');
const ranter = require('./ranter');
const config = require('./config/' + (process.env.RANT_ENV || 'live'));
const port = process.env.PORT || 3000;
const server = http.createServer();
const routes = new mapleTree.RouteTree();
const getit = require('getit');

function streamRant(sourceLocation, res) {
  debug('attempting to stream rant from: ' + sourceLocation);
  getit(sourceLocation, function(err, content) {
    var source = err ? pull.values([err]) : pull(
      ranter(content || '', config),
      // throttle updates
      pull.asyncMap(function(data, cb) {
        setTimeout(function() {
          cb(null, data);
        }, data.first ? 0 : data.delay || 100);
      })
    );

    pull(source, sse(res));
  });
}

const serveIndex = (req, res) => {
  res.writeHead(200, { 'content-type': 'text/html' });
  fs.createReadStream(__dirname + '/index.html').pipe(res);
};

routes.define('/rant/:user/:rant', function(req, res) {
  streamRant('https://gist.githubusercontent.com/' + this.params.user + '/' + this.params.rant + '/raw', res);
});

routes.define('/rant/:user', function(req, res) {
  streamRant(path.resolve(__dirname, 'rants', this.params.user), res);
});

routes.define('/rant.js', function(req, res) {
  browserify(__dirname + '/client/index.js').bundle().pipe(res);
});

routes.define('*', serveIndex);

server.on('request', function(req, res) {
  var match = routes.match(req.url);
  debug(`url: ${req.url}, have match: ${!!match}, is index html: ${match && match.fn === serveIndex}`);
  if (match && match.fn) {
    match.fn(req, res);
  }
});

server.listen(port);
