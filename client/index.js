var crel = require('crel');
var rant = new EventSource('/rant' + window.location.pathname);
var handlers = {};
var container;
var moment = require('moment');
var start = moment();

handlers.$end = function() {
  var msg = 'rant completed in ' + moment().diff(start, 'seconds') + ' seconds';

  rant.close();

  return crel('p', { class: 'section'},
    crel('strong', 'FIN'),
    crel('p', { class: 'done' }, msg)
  );
}

handlers.$section = function(message) {
  var section = crel('p', { class: 'section'});

  // TODO: stagger the display as per the original version
  message.data.forEach(function(parts) {
    section.appendChild(crel('strong', { class: parts[1] }, parts[0] + ' '));
  });

  return section;
};

handlers.$block = function(data) {
  return crel('pre', data);
};

handlers.$exception = function(data) {
  return crel('p', { class: 'exception' }, data);
};

handlers.$counter = function(data) {
  return crel('p', { class: 'heckle' }, data);
};

handlers.$ref = function(data) {
  var list = crel.apply(crel, ['ul'].concat(data.split('\n').map(function(line) {
    return crel('li', line);
  })));

  return crel('p', { class: 'references' }, crel('h3', 'References'), list);
};

handlers.$url = function(data) {
  return crel('iframe', {
    src: data,
    scrolling: 'no',
    border: 'no'
  });
};

rant.addEventListener('message', function(evt) {
  var message;
  var lines;
  var newContainer;
  var parts;
  var text;

  try {
    message = JSON.parse(evt.data);
  }
  catch (e) {
    console.log('could not parse data', evt.data);
    return;
  }

  if (handlers[message.type]) {
    newContainer = handlers[message.type].call(evt, message);
    if (newContainer) {
      container && document.body.removeChild(container);
      document.body.appendChild(container = newContainer);
    }
  }
});
